# JO2024

## Vidéo d'utilisation rapide :

<img src="public/presentation-application-realisee-avec-clipchamp_5pw5Wsvh.mp4" width="80%">

## Description

JO2024 est un projet en Symfony qui vise à fournir une plateforme pour les Jeux Olympiques de Paris en 2024. Notre objectif est de créer une expérience utilisateur intuitive et agréable, tout en fournissant des informations précises et à jour sur les événements, les athlètes et les résultats.

## Fonctionnalités

- Une page d'accueil dynamique avec les dernières nouvelles et les événements à venir
- Des pages dédiées à chaque sport, avec des descriptions, des règles et des historiques
- Des profils d'athlètes détaillés, avec des statistiques, des récompenses et des informations personnelles
- Un calendrier interactif pour tous les événements, avec des options de filtrage et de tri
- Des résultats en temps réel pour tous les événements, ainsi que des tableaux de médailles et des statistiques

## Installation

Pour installer ce projet sur votre ordinateur local, suivez ces étapes :

1. Ouvrez un terminal et naviguez jusqu'au répertoire où vous souhaitez installer le projet
2. Exécutez la commande suivante pour cloner le dépôt Git :
```
git clone https://gitlab.com/rayanSalhi/JO2024.git
```
3. Naviguez dans le répertoire du projet :
```
cd JO2024
```
4. Installez les dépendances requises en utilisant la commande suivante :
```
composer install
```
5. Configurez les paramètres de la base de données dans le fichier `.env`
6. Créez la base de données et les tables en utilisant la commande suivante :
```
php bin/console doctrine:schema:update --force
```
7. Chargez les données d'exemple en utilisant la commande suivante :
```
php bin/console doctrine:fixtures:load
```
8. Démarrez le serveur de développement en utilisant la commande suivante :
```
php bin/console server:start
```
9. Ouvrez votre navigateur et accédez à l'adresse suivante :
```
http://localhost:8000
```

## Contribution

Nous sommes ouverts aux contributions de la communauté. Si vous souhaitez contribuer à ce projet, veuillez suivre ces étapes :

1. Forker le dépôt Git
2. Créer une branche pour votre contribution
3. Effectuer vos modifications et commiter vos changements
4. Pousser vos modifications sur votre branche
5. Ouvrir une demande de pull sur le dépôt Git d'origine


## Statut du projet

Le projet est actuellement en cours de développement actif. Nous prévoyons de publier une version bêta publique dans les prochains mois, suivie d'une version complète avant les Jeux Olympiques de Paris en 2024.