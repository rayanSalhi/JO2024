<?php

namespace App\Classe;
use App\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


use Symfony\Component\HttpFoundation\RequestStack;

class Cart
{
    private $requestStack;
    private $entityManager;


    public function __construct(ORMEntityManagerInterface $entityManager,RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

        // Méthode privée pour modifier la quantité d'un ticket dans le panier
        private function modifyQuantity(Event $event, SessionInterface $session, string $type, int $increment)
        {
            $panier = $session->get('panier', []);
    
            foreach ($panier as &$item) {
                if ($item["event_id"] === $event->getId() && $item["type"] === $type) {
                    $item['quantity'] += $increment;
                    if ($item['quantity'] <= 0) {
                        unset($item);
                    }
                    break;
                }
            }
    
            $session->set('panier', $panier);
        }

    // Méthode privée pour ajouter un ticket au panier
    private function addToCart(Event $event, SessionInterface $session, string $type, float $price)
    {
        $panier = $session->get('panier', []);

        $found = false;
        foreach ($panier as &$item) {
            if ($item["event_id"] === $event->getId() && $item["type"] === $type) {
                $item['quantity']++;
                $found = true;
                break;
            }
        }

        if (!$found) {
            $panier[] = [
                'event_id' => $event->getId(),
                'type' => $type,
                'quantity' => 1,
                'price' => $price,
            ];
        }

        $session->set('panier', $panier);
    }



        // Méthode privée pour supprimer un ticket du panier
        private function deleteFromCart(Event $event, SessionInterface $session, string $type)
        {
            $panier = $session->get('panier', []);
    
            foreach ($panier as $key => &$item) {
                if ($item["event_id"] === $event->getId() && $item["type"] === $type) {
                    unset($panier[$key]);
                    break;
                }
            }
    
            $session->set('panier', $panier);
        }
}





