<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class SessionController extends AbstractController
{

    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    #[Route('/', name: 'session')]
    public function index(RequestStack $requestStack): Response
    {
        $session = $this->requestStack->getSession();
        if($session->has('nbVisite')){
            $nbVisite= $session->get('nbVisite')+1;
            $session->set('nbVisite',$nbVisite);

        }else{
            $nbVisite=1;
            $session->set('nbVisite',$nbVisite);
            
        }

        return $this->render('home/index.html.twig',);
    }
}
