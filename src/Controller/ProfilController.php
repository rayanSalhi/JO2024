<?php

namespace App\Controller;

use App\Form\AdresseType;
use App\Form\ProfilType;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProfilController extends AbstractController
{
    private $entityManager;

    public function __construct(ORMEntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/home/profil', name: 'app_profil')]
    public function password(Request $request, UserPasswordHasherInterface $encoder)
    {
        // Initialisation de la notification
        $notification = null;

        // Récupération de l'utilisateur actuel
        $user = $this->getUser();

        // Création du formulaire de modification de mot de passe
        $form = $this->createForm(ProfilType::class, $user);

        // Traitement de la soumission du formulaire
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Récupération de l'ancien mot de passe
            $old_pwd = $form->get('old_password')->getData();

            // Vérification si l'ancien mot de passe est correct
            if ($encoder->isPasswordValid($user, $old_pwd)) {
                // Récupération et hachage du nouveau mot de passe
                $new_pwd = $form->get('new_password')->getData();
                $password = $encoder->hashPassword($user, $new_pwd);

                // Mise à jour du mot de passe de l'utilisateur
                $user->setPassword($password);
                $this->entityManager->flush();

                // Notification de succès
                $notification = "Votre mot de passe a été mis à jour.";
            } else {
                // Notification d'échec
                $notification = "Votre mot de passe actuel est incorrect.";
            }
        }

        // Rendu de la vue Twig avec le formulaire et la notification
        return $this->render('profil/profil.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification
        ]);
    }

    #[Route('/home/profil/', name: 'app_profil')]
    public function adresse(Request $request, EntityManagerInterface $entityManager)
    {
        // Récupération de l'utilisateur actuel
        $user = $this->getUser();

        // Création du formulaire de modification d'adresse
        $form = $this->createForm(ProfilType::class, $user);

        // Traitement de la soumission du formulaire
        $form->handleRequest($request);

        // Persist et flush pour enregistrer les modifications de l'utilisateur
        $entityManager->persist($user);
        $entityManager->flush();

        // Rendu de la vue Twig avec le formulaire
        return $this->render('profil/profil.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
