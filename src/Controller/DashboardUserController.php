<?php

namespace App\Controller;

use App\Repository\OrderRepository;
use App\Repository\TicketRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DashboardUserController extends AbstractController
{
    #[Route('/dashboard/user', name: 'app_dashboard_user')]
    public function index(TicketRepository $ticketRepository, OrderRepository $orderRepository): Response
    {
        // Vérification des autorisations : seul l'utilisateur authentifié en tant que ROLE_USER ou ROLE_ADMIN peut accéder à ce tableau de bord.
        $this->denyAccessUnlessGranted('ROLE_USER', 'ROLE_ADMIN');

        // Obtention de l'ID de l'utilisateur actuel.
        $userId = $this->getUser()->getId();

        // Récupération des commandes associées à cet utilisateur.
        $myOrders = $orderRepository->findBy(['userId' => $userId]);

        // Initialisation du tableau pour stocker les informations des billets de l'utilisateur.
        $myTickets = [];

        // Pour chaque commande de l'utilisateur, récupération des billets associés.
        foreach ($myOrders as $order) {
            $tickets = $ticketRepository->findBy(['orders_id' => $order->getId()]);

            // Pour chaque billet, extraction des informations pertinentes et ajout au tableau des billets.
            foreach ($tickets as $ticket) {
                $myTickets[] = [
                    'reference' => $order->getReference(),
                    'idTicket' => $ticket->getId(),
                    'image' => $ticket->getEvent()->getIllustration(),
                    'sport' => $ticket->getEvent()->getCategory()->getName(),
                    'description' => $ticket->getEvent()->getDescription(),
                    'type' => $ticket->getType(),
                    'price' => $ticket->getPrice(),
                    'date' => $ticket->getEvent()->getDate(),
                ];
            }
        }

        // Rendu de la vue Twig avec les données récupérées.
        return $this->render('dashboard_user/index.html.twig', [
            'myOrders' => $myOrders,
            'myTickets' => $myTickets,
        ]);
    }
}
