<?php

namespace App\Controller;

use App\Classe\Search;
use App\Entity\Event;
use App\Form\SearchType;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class EventController extends AbstractController
{
    private $entityManager;

    public function __construct(ORMEntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/evenement', name: 'events')]
    public function index(Request $request): Response
    {
        // Récupération de tous les événements depuis la base de données
        $events = $this->entityManager->getRepository(Event::class)->findAll();

        // Création d'une instance de la classe Search et d'un formulaire de recherche
        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);

        // Gestion de la soumission du formulaire
        $form->handleRequest($request);

        // Si le formulaire est soumis et valide, récupération des événements filtrés
        if ($form->isSubmitted() && $form->isValid()) { 
            $events = $this->entityManager->getRepository(Event::class)->findWithSearch($search);
        }

        // Rendu de la vue Twig avec les événements et le formulaire de recherche
        return $this->render('event/index.html.twig', [
            'events' => $events,
            'form' => $form->createView()
        ]);
    }

    #[Route('/evenement/{slug}', name: 'event')]
    public function show($slug):Response
    {
        // Récupération d'un événement par son slug depuis la base de données
        $event = $this->entityManager->getRepository(Event::class)->findOneBySlug($slug);

        // Redirection vers la liste des événements si l'événement n'est pas trouvé
        if (!$event) {
            return $this->redirectToRoute('events');
        }

        // Rendu de la vue Twig pour afficher les détails de l'événement
        return $this->render('event/show.html.twig', [
            'event' => $event
        ]);
    }
}
