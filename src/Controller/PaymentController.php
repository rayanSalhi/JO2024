<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Stripe;
use Stripe\Checkout\Session;
use App\Entity\Order;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use DateTimeImmutable;
use App\Entity\Ticket;


class PaymentController extends AbstractController
{
    private EntityManagerInterface $em;
    private UrlGeneratorInterface $generator;

    public function __construct(EntityManagerInterface $em, UrlGeneratorInterface $generator)
    {
        $this->em = $em;
        $this->generator = $generator;
    }

    #[Route('/order/create-session-stripe', name: 'payment_stripe')]
    public function stripeCheckout(SessionInterface $session, EventRepository $EventRepository, EntityManagerInterface $em): RedirectResponse
    {
        $session->remove('panier');
        $panier = $session->get('order', []);

        
        //Si le panier n'est pas vide on creer la commande
        $order = new Order();
        $order->setUserId($this->getUser());
        $reference = uniqid();
        $order->setReference($reference);
        $order->setCreatedAt(new DateTimeImmutable('now'));

        //On parcours le panier pour créer les tickets relié a la commande
        foreach ($panier as $item) {
            for ($i = $item['quantity']; $i > 0; $i--) {

                $event = $EventRepository->find($item['event_id']);
                $price = $item['price'];
                $type = $item['type'];
                //On créer les tickets
                $ticket = (new Ticket())
                        ->setEvent($event)
                        ->setType($type)
                        ->setPrice($price)
                        ->setKeyTicket(uniqid());

                $order->addTicket($ticket);
            }
        }

        // persist et flush
        $em->persist($order);
        $em->flush();


        $productStripe = [];


        $order = $this->em->getRepository(Order::class)->findOneBy(['reference' => $reference]);
        if (!$order) {
            return $this->redirectToRoute('cart');
        }



        $cpt = 0;
        foreach ($order->getTickets()->getValues() as $event) {
            $cpt +=1;
            $price = (int) ($event->getPrice() * 100);
            $productStripe[] = [
                'price_data' => [
                    'currency' => 'eur',
                    'unit_amount' => $price / 100 ,
                    'product_data' => [
                        'name' => $event->getEvent()->getDescription() . " Ticket #" . $cpt
                    ]
                ],
                 'quantity' => 1
            ];
        }

       


        $stripeSecretKey = 'sk_test_51P8gHx04lAq02flf5NyAtyx0GbZFpLdgMeEKkXv5ATQZt3oKV5SD7dP08wBcGllxm1hOzWKG4JaonGBv0KR2ah8A00zAGFY5O3';
        Stripe::setApiKey($stripeSecretKey);


        $checkout_session = Session::create([
            'customer_email' => $this->getUser()->getEmail(),
            'payment_method_types' => ['card'],
            'line_items' => [[
                $productStripe
            ]],
            'mode' => 'payment',
            'success_url' => $this->generator->generate('payment_success', [
                'reference' => $order->getReference()
            ], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => 'https://' . $this->generator->generate('payment_error', [
                'reference' => $order->getReference()
            ], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);

        

        return new RedirectResponse($checkout_session->url);
    }


    #[Route('/order/success/{reference}', name: 'payment_success')]
    public function stripeSuccess($reference ,Request $request): Response
    {
       return $this->render('order/success.html.twig');
    }


    #[Route('/order/error/{reference}', name: 'payment_error')]
    public function stripeError($reference ,Request $request): Response
    {

        return $this->render('order/error.html.twig');
    }
}
