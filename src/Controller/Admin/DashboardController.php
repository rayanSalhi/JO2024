<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Event;
use App\Entity\User;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    protected $eventRepository;
    protected $userRepository;
    /// creation des variable event et user
    public function __construct(UserRepository $userRepository,EventRepository $eventRepository)
    {
        $this->eventRepository =$eventRepository;
        $this->userRepository=$userRepository;

        
    }
    
    /// ici nous renveront tous sur vue du Dashborad admin
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        return $this->render('admin/dashboard.html.twig',[
            ///Envois des fonctions sur la vue
            'countAllUser' => $this->userRepository->countAllUser(),
            'countAllEvent' => $this->eventRepository->countAllEvent(),

        ]);
        
    }
    /// Ici nous allons configurer le dashboard admin
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Jeux olympiques de Paris 2024');
    }

    public function configureMenuItems(): iterable
    {
        /// Ici nous avons tous les onglet qui seront liée au dashbord admin 
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Catgories', 'fas fa-school', Category::class);
        yield MenuItem::linkToCrud('Utilisateur', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Événement', 'fas fa-user', Event::class);

    }
}
