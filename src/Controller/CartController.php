<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/mon-panier', name: 'cart_')] // Définit le préfixe des routes de ce contrôleur
class CartController extends AbstractController
{
    // Tableau associatif des multiplicateurs de prix pour chaque type d'événement
    private const TYPES = [
        'solo' => 1,
        'duo' => 1.8,
        'family' => 3.4
    ];

    // Affiche le contenu du panier
    #[Route('/', name: 'index')]
    public function index(SessionInterface $session, EventRepository $eventRepository)
    {
        $cart = $session->get('panier', []); // Récupère le panier depuis la session
        $data = []; // Tableau pour stocker les données à afficher dans la vue
        $total = 0; // Variable pour calculer le total du panier

        // Parcourt chaque élément du panier
        foreach ($cart as $item) {
            // Récupère l'événement correspondant à l'ID de l'élément du panier depuis le repository
            if ($event = $eventRepository->find($item['event_id'])) {
                // Ajoute les données de l'événement et de l'élément du panier au tableau de données
                $data[] = [
                    'event' => $event,
                    'type' => $item['type'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                ];
                // Calcule le total en multipliant le prix par la quantité pour chaque élément du panier
                $total += $item['price'] * $item['quantity'];
            }
        }

        // Affiche le contenu du panier dans la vue 'cart/index.html.twig'
        return $this->render('cart/index.html.twig', compact('data', 'total'));
    }

    // Ajoute un événement au panier
    #[Route('/add/{type}/{id}', name: 'add')]
    public function add(Event $event, SessionInterface $session, string $type)
    {
        $cart = $session->get('panier', []); // Récupère le panier depuis la session

        $priceMultiplier = self::TYPES[$type] ?? 1; // Récupère le multiplicateur de prix correspondant au type d'événement
        $found = false; // Variable pour indiquer si l'événement est déjà présent dans le panier

        // Parcourt chaque élément du panier
        foreach ($cart as &$item) {
            // Vérifie si l'événement est déjà présent dans le panier
            if ($item['event_id'] === $event->getId() && $item['type'] === $type) {
                // Incrémente la quantité si l'événement est déjà présent dans le panier
                $item['quantity']++;
                $found = true; // Indique que l'événement a été trouvé dans le panier
                break;
            }
        }

        // Si l'événement n'est pas trouvé dans le panier, l'ajoute avec une quantité de 1
        if (!$found) {
            $cart[] = [
                'event_id' => $event->getId(),
                'type' => $type,
                'quantity' => 1,
                'price' => $event->getPrice() * $priceMultiplier, // Calcule le prix en fonction du multiplicateur de prix
            ];
        }

        // Met à jour le panier dans la session
        $session->set('panier', $cart);

        // Redirige vers la page du panier
        return $this->redirectToRoute('cart');
    }

    // Modifie la quantité d'un événement dans le panier ou le supprime
    #[Route('/remove/{type}/{id}', name: 'remove')]
    #[Route('/delete/{type}/{id}', name: 'delete')]
    public function modifyCart(Event $event, SessionInterface $session, string $type, int $increment = -1)
    {
        $cart = $session->get('panier', []); // Récupère le panier depuis la session

        // Parcourt chaque élément du panier
        foreach ($cart as $key => &$item) {
            // Vérifie si l'événement correspondant au type et à l'ID est trouvé dans le panier
            if ($item['event_id'] === $event->getId() && $item['type'] === $type) {
                // Modifie la quantité de l'événement dans le panier en ajoutant ou en soustrayant l'incrément spécifié
                $item['quantity'] += $increment;
                // Si la quantité devient inférieure ou égale à zéro, supprime l'événement du panier
                if ($item['quantity'] <= 0) {
                    unset($cart[$key]);
                }
                break; // Sort de la boucle après avoir trouvé l'événement dans le panier
            }
        }

        // Met à jour le panier dans la session
        $session->set('panier', $cart);

        // Redirige vers la page du panier
        return $this->redirectToRoute('cart');
    }

    // Vide le panier
    #[Route('/empty', name: 'empty')]
    public function empty(SessionInterface $session)
    {
        $session->remove('panier'); // Supprime le panier de la session
        // Redirige vers la page du panier
        return $this->redirectToRoute('cart');
    }
}
