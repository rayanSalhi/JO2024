<?php

namespace App\Controller;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Event;
use App\Form\AdresseType;
use App\Form\ProfilType;
use Doctrine\ORM\EntityManagerInterface as ORMEntityManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class HomeController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/', name: 'home')]
    public function index(Request $request): Response
    {
        // Récupérer la session et gérer le nombre de visites
        $session = $request->getSession();
        $nbVisite = $session->get('nbVisite', 0);
        $nbVisite++;
        $session->set('nbVisite', $nbVisite);

        // Récupérer les événements depuis la base de données
        $events = $this->entityManager->getRepository(Event::class)->findBy([], ['id' => 'ASC']);

        // Rendre la vue avec les données nécessaires
        return $this->render('home/index.html.twig', [
            'nbVisite' => $nbVisite,
            'events' => $events
        ]);
    }
}