<?php

namespace App\Controller;

use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\TicketRepository;
use Symfony\Component\Routing\Attribute\Route;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Color\Color;

class QrCodeController extends AbstractController
{
    

    #[Route('/pdf/{id}', name: 'pdf')]
    public function index($id, TicketRepository $ticketRepository): Response
    {
        // Récupération du ticket via son ID
        $ticket = $ticketRepository->findOneBy(array('id' => $id));
        $ref = $ticket->getOrdersId()->getReference();

        // Récupération de la clé du ticket et de la clé du compte utilisateur
        $keyticket = $ticket->getKeyTicket();
        $keyaccount = $ticket->getOrdersId()->getUserId()->getKeyAccount();

        // Concaténation des clés pour créer une clé de chiffrement unique
        $skcode = $keyticket . $keyaccount;

        // Configuration de l'encodage pour le QR code
        $method = "AES-128-CTR";
        $key = "encryptionKey123";
        $options = 0;
        $iv = '1234567891011121';
        $encryptedData = openssl_encrypt($skcode, $method, $key, $options, $iv);
        $encryptedDataBase64 = base64_encode($encryptedData);

        // Création du QR code
        $encoding = new Encoding('UTF-8');
        $qrCode = new QrCode('127.0.0.1:8000/qrcode/' . $encryptedDataBase64);
        $qrCode->setSize(200);
        $qrCode->setMargin(10);
        $qrCode->setEncoding($encoding);
        $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::High);
        $qrCode->setForegroundColor(new Color(0, 0, 0));
        $qrCode->setBackgroundColor(new Color(255, 255, 255));

        // Génération de l'image du QR code en format Data URI
        $writer = new PngWriter();
        $result = $writer->write($qrCode);
        $dataUri = $result->getDataUri();

        // Récupération des informations du ticket pour l'affichage dans le PDF
        $myTicket[] = [
            'reference' => $ticket->getOrdersId()->getReference(),
            'idTicket' => $ticket->getId(),
            'sport' => $ticket->getEvent()->getCategory()->getName(),
            'description' => $ticket->getEvent()->getDescription(),
            'type' => $ticket->getType(),
            'price' => $ticket->getPrice(),
            'date' => $ticket->getEvent()->getDate(),
        ];
        $imageHtml = $ticket->getEvent()->getIllustration();

        // Configuration pour Dompdf
        $options = New Options();
        $options->setChroot(realpath(''));

        // Rendu de la vue Twig pour le contenu HTML du PDF
        $html = $this->renderView('pdf/index.html.twig', [
            'ticket' => $myTicket,
            'qrcode' => $dataUri,
            'image' => $imageHtml,
        ]);

        // Initialisation de Dompdf et génération du PDF
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        // Envoi du PDF en réponse HTTP
        $dompdf->stream('ticket' . $ref, ["attachement" => false], 200, ['Content-Type' => 'application/pdf']);
        return new Response('', 200, [
            'Content-Type' => 'application/pdf',
        ]);
    }


    #[Route('/qrcode/{key}', name: 'app_qrcode')]
    public function decode($key, TicketRepository $ticketRepository): Response
    {
        // Décodage de la clé du QR code
        $encryptedData = base64_decode($key);
        $method = "AES-128-CTR";
        $keycrypt = "encryptionKey123";
        $options = 0;
        $iv = '1234567891011121';
        $decryptedData = openssl_decrypt($encryptedData, $method, $keycrypt, $options, $iv);
        $decryptedKeys = str_split($decryptedData, 13);

        // Récupération du ticket correspondant à la clé déchiffrée
        $ticket = $ticketRepository->findOneBy(array('keyTicket' => $decryptedKeys[0]));
        $reponse = "TICKET NON OFFICIEL";


        // Vérification si le ticket est valide
        if ($ticket) {
            if ($ticket->getOrdersId()->getUserId()->getKeyAccount() === $decryptedKeys[1]) {
                $nom = $ticket->getOrdersId()->getUserId()->getLastname();
                $prenom = $ticket->getOrdersId()->getUserId()->getFirstname();
                $mail = $ticket->getOrdersId()->getUserId()->getEmail();
                $reponse = "TICKET OFFICIEL";
                
                // Rendu de la vue Twig pour afficher les informations du ticket
                return $this->render('qrcode/index.html.twig', [
                    'key' => $decryptedData,
                    'reponse' => $reponse,
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'mail' => $mail,
                ]);
            }
        }
   
        // Rendu de la vue Twig pour indiquer que le ticket n'est pas valide
        return $this->render('qrcode/index.html.twig', [
            'reponse' => $reponse,
        ]);
    }
}
