<?php

namespace App\Tests\Unit\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\Event;
use App\Entity\Category;

class EventTest extends TestCase
{
    public function testEventProperties()
    {
        $event = new Event();

        $event->setName("Event Name")
              ->setSlug("event-name")
              ->setIllustration("illustration.jpg")
              ->setSubtitle("Event Subtitle")
              ->setDescription("Event Description")
              ->setPrice(10.99);

        $this->assertSame("Event Name", $event->getName());
        $this->assertSame("event-name", $event->getSlug());
        $this->assertSame("illustration.jpg", $event->getIllustration());
        $this->assertSame("Event Subtitle", $event->getSubtitle());
        $this->assertSame("Event Description", $event->getDescription());
        $this->assertSame(10.99, $event->getPrice());
    }

    public function testCategoryRelation()
    {
        $event = new Event();
        $category = new Category();
        $category->setName("Category Name");

        $event->setCategory($category);

        $this->assertInstanceOf(Category::class, $event->getCategory());
        $this->assertSame($category, $event->getCategory());
    }
}
