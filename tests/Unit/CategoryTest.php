<?php

namespace App\Tests\Unit\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\Category;
use App\Entity\Event;

class CategoryTest extends TestCase
{
    public function testCategoryProperties()
    {
        $category = new Category();

        $category->setName("Category Name");

        $this->assertSame("Category Name", $category->getName());
    }

    public function testEventsCollection()
    {
        $category = new Category();
        $event1 = new Event();
        $event2 = new Event();

        $category->addEvent($event1);
        $category->addEvent($event2);

        $events = $category->getEvents();

        $this->assertInstanceOf(\Doctrine\Common\Collections\Collection::class, $events);
        $this->assertCount(2, $events);
        $this->assertTrue($events->contains($event1));
        $this->assertTrue($events->contains($event2));

        $category->removeEvent($event1);

        $this->assertCount(1, $category->getEvents());
        $this->assertFalse($category->getEvents()->contains($event1));
    }
}
