<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Order;
use App\Entity\User;
use App\Entity\Ticket;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;

class OrderTest extends TestCase
{
    public function testIsTrue()
    {
        $order = new Order();
        $user = new User();
        $createdAt = new DateTimeImmutable('2023-01-01 00:00:00');

        $order->setReference("ORD12345")
              ->setUserId($user)
              ->setCreatedAt($createdAt);

        $this->assertTrue($order->getReference() === "ORD12345");
        $this->assertTrue($order->getUserId() === $user);
        $this->assertTrue($order->getCreatedAt() === $createdAt);
    }

    public function testIsFalse()
    {
        $order = new Order();
        $user = new User();
        $createdAt = new DateTimeImmutable('2023-01-01 00:00:00');

        $order->setReference("ORD12345")
              ->setUserId($user)
              ->setCreatedAt($createdAt);

        $this->assertFalse($order->getReference() === "WRONG_REF");
        $this->assertFalse($order->getUserId() === null);
        $this->assertFalse($order->getCreatedAt() === new DateTimeImmutable('2022-01-01 00:00:00'));
    }

    public function testIsEmpty()
    {
        $order = new Order();

        $this->assertEmpty($order->getReference());
        $this->assertNull($order->getUserId());
        $this->assertNull($order->getCreatedAt());
        $this->assertInstanceOf(ArrayCollection::class, $order->getTickets());
        $this->assertEmpty($order->getTickets());
    }

    public function testAddRemoveTicket()
    {
        $order = new Order();
        $ticket = new Ticket();

        $order->addTicket($ticket);

        $this->assertTrue($order->getTickets()->contains($ticket));
        $this->assertSame($order, $ticket->getOrdersId());

        $order->removeTicket($ticket);

        $this->assertFalse($order->getTickets()->contains($ticket));
        $this->assertNull($ticket->getOrdersId());
    }
}
?>
