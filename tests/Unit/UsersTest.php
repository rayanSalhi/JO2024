<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\User;

class UserTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail("true@gmail.com")
             ->setRoles(["ROLE_USER"])
             ->setPassword("password")
             ->setFirstName("firstname")
             ->setLastName("lastname")
             ->setKeyAccount("keyaccount")
             ->setAdresse("123 rue de Paris")
             ->setCP("75000")
             ->setTelephone("0123456789")
             ->setCivilite("Mr")
             ->setVille("Paris");

        $this->assertTrue($user->getEmail() === 'true@gmail.com');
        $this->assertEqualsCanonicalizing(["ROLE_USER"], $user->getRoles());
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getFirstName() === 'firstname');
        $this->assertTrue($user->getLastName() === 'lastname');
        $this->assertTrue($user->getKeyAccount() === 'keyaccount');
        $this->assertTrue($user->getAdresse() === '123 rue de Paris');
        $this->assertTrue($user->getCP() === '75000');
        $this->assertTrue($user->getTelephone() === '0123456789');
        $this->assertTrue($user->getCivilite() === 'Mr');
        $this->assertTrue($user->getVille() === 'Paris');
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail("true@gmail.com")
             ->setRoles(["ROLE_USER"])
             ->setPassword("password")
             ->setFirstName("firstname")
             ->setLastName("lastname")
             ->setKeyAccount("keyaccount")
             ->setAdresse("Adresse")
             ->setCP("CP")
             ->setTelephone("Telephone")
             ->setCivilite("Civilite")
             ->setVille("Ville");

        $this->assertFalse($user->getEmail() === 'false@gmail.com');
        $this->assertFalse($user->getRoles() === ["false"]);
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getFirstName() === 'false');
        $this->assertFalse($user->getLastName() === 'false');
        $this->assertFalse($user->getKeyAccount() === 'false');
        $this->assertFalse($user->getAdresse() === 'false');
        $this->assertFalse($user->getCP() === 'false');
        $this->assertFalse($user->getTelephone() === 'false');
        $this->assertFalse($user->getCivilite() === 'false');
        $this->assertFalse($user->getVille() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $user->setEmail('')
             ->setRoles([""])
             ->setPassword("")
             ->setFirstName("")
             ->setLastName("")
             ->setKeyAccount("")
             ->setAdresse("")
             ->setCP("")
             ->setTelephone("")
             ->setCivilite("")
             ->setVille("");

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getFirstName());
        $this->assertEmpty($user->getLastName());
        $this->assertEmpty($user->getKeyAccount());
        $this->assertEmpty($user->getAdresse());
        $this->assertEmpty($user->getCP());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getCivilite());
        $this->assertEmpty($user->getVille());
    }
}
?>

