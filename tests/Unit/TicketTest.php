<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Ticket;
use App\Entity\Event;
use App\Entity\Order;

class TicketTest extends TestCase
{
    public function testIsTrue()
    {
        $ticket = new Ticket();
        $event = new Event();
        $order = new Order();

        $ticket->setType("VIP")
               ->setEvent($event)
               ->setPrice(150.0)
               ->setOrdersId($order)
               ->setKeyTicket("keyticket");

        $this->assertTrue($ticket->getType() === "VIP");
        $this->assertTrue($ticket->getEvent() === $event);
        $this->assertTrue($ticket->getPrice() === 150.0);
        $this->assertTrue($ticket->getOrdersId() === $order);
        $this->assertTrue($ticket->getKeyTicket() === "keyticket");
    }

    public function testIsFalse()
    {
        $ticket = new Ticket();
        $event = new Event();
        $order = new Order();

        $ticket->setType("VIP")
               ->setEvent($event)
               ->setPrice(150.0)
               ->setOrdersId($order)
               ->setKeyTicket("keyticket");

        $this->assertFalse($ticket->getType() === "Regular");
        $this->assertFalse($ticket->getEvent() === null);
        $this->assertFalse($ticket->getPrice() === 200.0);
        $this->assertFalse($ticket->getOrdersId() === null);
        $this->assertFalse($ticket->getKeyTicket() === "wrongkey");
    }

    public function testIsEmpty()
    {
        $ticket = new Ticket();

        $ticket->setType(null)
               ->setEvent(null)
               ->setPrice(null)
               ->setOrdersId(null)
               ->setKeyTicket("");

        $this->assertEmpty($ticket->getType());
        $this->assertNull($ticket->getEvent());
        $this->assertNull($ticket->getPrice());
        $this->assertNull($ticket->getOrdersId());
        $this->assertEmpty($ticket->getKeyTicket());
    }
}
?>
