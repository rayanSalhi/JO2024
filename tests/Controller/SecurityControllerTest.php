<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{
    public function testLogin(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Envoyer une requête GET à la route "app_login"
        $client->request('GET', '/connexion');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le contenu de la réponse contient le titre de la page
        $this->assertSelectorTextContains('h1', 'Connexion');

        // Remplir le formulaire de connexion avec des données valides
        $client->submitForm('Se connecter', [
            '_username' => 'test@example.com',
            '_password' => 'password123',
        ]);

        // Vérifier que la redirection vers la page d'accueil a bien été effectuée
        $this->assertResponseRedirects('/');

        // Vérifier que l'utilisateur est bien connecté
        // Vous pouvez par exemple vérifier que la réponse contient un élément qui n'est visible que pour les utilisateurs connectés
    }

    public function testLogout(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Simuler une connexion en définissant un token d'authentification dans la session
        $session = $client->getContainer()->get('session');
        $session->set('_security_main', 'token_value');
        $session->save();

        // Envoyer une requête GET à la route "app_logout"
        $client->request('GET', '/deconnexion');

        // Vérifier que la redirection vers la page de connexion a bien été effectuée
        $this->assertResponseRedirects('/connexion');

        // Vérifier que l'utilisateur est bien déconnecté
        // Vous pouvez par exemple vérifier que la réponse ne contient pas un élément qui n'est visible que pour les utilisateurs connectés
    }
}
