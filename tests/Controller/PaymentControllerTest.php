<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\Event;
use App\Entity\Ticket;

class PaymentControllerTest extends WebTestCase
{
    public function testStripeCheckout(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Créer un utilisateur et le persister en base de données
        $user = new User();
        $user->setEmail('test@example.com');
        $user->setPassword('password');
        $user->setRoles(['ROLE_USER']);
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');
        $entityManager->persist($user);
        $entityManager->flush();

        // Créer un événement et le persister en base de données
        $event = new Event();
        $event->setName('Test Event');
        $event->setPrice(10);
        // ... ajouter d'autres propriétés si nécessaire
        $entityManager->persist($event);
        $entityManager->flush();

        // Connecter le client en tant qu'utilisateur
        $client->loginUser($user);

        // Ajouter un élément au panier en utilisant la session
        $session = $client->getContainer()->get('session');
        $panier = $session->get('order', []);
        $panier[] = [
            'event_id' => $event->getId(),
            'type' => 'Solo',
            'price' => $event->getPrice(),
            'quantity' => 2,
        ];
        $session->set('order', $panier);

        // Envoyer une requête GET à la route "payment_stripe"
        $client->request('GET', '/order/create-session-stripe');

        // Vérifier que le code de statut de la réponse est 302 (redirection)
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);

        // Vérifier que la redirection pointe vers l'URL de paiement Stripe
        $this->assertResponseRedirectsTo('https://checkout.stripe.com/c/pay');

        // Vérifier que la commande a été créée et que les tickets ont été ajoutés
        $order = $entityManager->getRepository(Order::class)->findOneBy(['user' => $user]);
        $this->assertNotNull($order);
        $this->assertCount(2, $order->getTickets());
        $this->assertSame('Solo', $order->getTickets()[0]->getType());
        $this->assertSame(10, $order->getTickets()[0]->getPrice());
    }
}
