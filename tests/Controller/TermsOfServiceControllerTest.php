<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TermsOfServiceControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Envoyer une requête GET à la route "app_terms_of_service"
        $client->request('GET', '/terms-of-service');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le contenu de la réponse contient le titre de la page
        $this->assertSelectorTextContains('h1', 'Terms of Service');
    }
}
