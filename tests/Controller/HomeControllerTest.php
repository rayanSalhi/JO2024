<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class HomeControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Envoyer une requête GET à la route "home"
        $client->request('GET', '/');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le contenu de la réponse contient le texte "Bienvenue sur notre site"
        $this->assertSelectorTextContains('h1', 'Bienvenue sur notre site');



        // Vérifier que la session a été initialisée et contient le nombre de visites
        $session = $client->getContainer()->get('session');
        $this->assertTrue($session->isStarted());
        $this->assertEquals(1, $session->get('nbVisite'));
    }
}
