<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DashboardUserControllerTest extends WebTestCase
{
    public function testDashboardUser()
    {
        
        
        // Créer un client anonyme
        $client = static::createClient();
        

        // Tenter d'accéder à la page de tableau de bord utilisateur
        $client->request('GET', '/dashboard/user');

        // Vérifier que la réponse est une redirection vers la page de connexion
        $this->assertResponseRedirects('/login');

        // Créer un client authentifié en tant qu'utilisateur
        $client = static::createClient();
        $user = static::$container->get('doctrine')->getRepository('App:User')->findOneBy(['email' => 'user@example.com']);
        $client->loginUser($user);

        // Tenter d'accéder à la page de tableau de bord utilisateur
        $client->request('GET', '/dashboard/user');

        // Vérifier que la réponse a le bon code de statut HTTP (200 OK)
        $this->assertResponseIsSuccessful();

        // Vérifier que la réponse contient certains éléments attendus, comme le titre de la page et les en-têtes de table
        $this->assertSelectorTextContains('h1', 'Mon tableau de bord');
        $this->assertSelectorTextContains('th', 'Référence');
        $this->assertSelectorTextContains('th', 'Type');
        $this->assertSelectorTextContains('th', 'Prix');
        $this->assertSelectorTextContains('th', 'Date');

        // Vérifier que les billets de l'utilisateur sont affichés dans le tableau
        $tickets = $user->getTickets();
        foreach ($tickets as $ticket) {
            $this->assertSelectorTextContains('td', $ticket->getEvent()->getTitle());
            $this->assertSelectorTextContains('td', $ticket->getType());
            $this->assertSelectorTextContains('td', $ticket->getPrice());
            $this->assertSelectorTextContains('td', $ticket->getEvent()->getDate()->format('d/m/Y'));
        }
    }
}
