<?php

namespace App\Tests\Controller;
use App\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class EventControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        // Crée un client pour simuler une requête HTTP
        $client = static::createClient();

        // Envoie une requête GET à la route "events"
        $client->request('GET', '/evenement');

        // Vérifie que le code de statut HTTP de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifie que la réponse contient le texte "Événements à venir"
        $this->assertSelectorTextContains('h1', 'Événements à venir');

    }

    public function testShow(): void
    {
        // Crée un client pour simuler une requête HTTP
        $client = static::createClient();

        // Récupère le premier événement de la base de données
        $event = static::getContainer()->get('doctrine')->getRepository(Event::class)->findOneBy([]);

        // Vérifie qu'un événement a été trouvé
        $this->assertNotNull($event);

        // Envoie une requête GET à la route "event" avec le slug de l'événement
        $client->request('GET', '/evenement' . $event->getSlug());

        // Vérifie que le code de statut HTTP de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifie que la réponse contient le nom de l'événement
        $this->assertSelectorTextContains('h1', $event->getName());

        // Vérifie que la réponse contient la description de l'événement
        $this->assertSelectorTextContains('p', $event->getDescription());
    }
}
