<?php


namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SessionControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Envoyer une requête GET à la route "session"
        $client->request('GET', '/');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que la session a été initialisée avec la bonne valeur
        $this->assertSame(1, $client->getContainer()->get('session')->get('nbVisite'));

        // Envoyer une deuxième requête pour simuler une nouvelle visite
        $client->request('GET', '/');

        // Vérifier que la session a été mise à jour avec la bonne valeur
        $this->assertSame(2, $client->getContainer()->get('session')->get('nbVisite'));
    }
}
