<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;

class ProfilControllerTest extends WebTestCase
{
    public function testPassword(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Créer un utilisateur et le persister en base de données
        $user = new User();
        $user->setEmail('test@example.com');
        $user->setPassword('password');
        $user->setRoles(['ROLE_USER']);
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');
        $entityManager->persist($user);
        $entityManager->flush();

        // Connecter le client en tant qu'utilisateur
        $client->loginUser($user);

        // Envoyer une requête GET à la route "app_profil"
        $client->request('GET', '/home/profil');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le formulaire de modification de mot de passe est présent dans la réponse
        $this->assertSelectorExists('form[name=profil]');

        // Simuler la soumission du formulaire avec des données valides
        $client->request('POST', '/home/profil', [
            'profil' => [
                'old_password' => 'password',
                'new_password' => 'new-password',
            ],
        ]);

        // Vérifier que la réponse contient un message de succès
        $this->assertSelectorTextContains('div.notification', 'Votre mot de passe a été mis à jour.');

        // Vérifier que le mot de passe de l'utilisateur a été mis à jour en base de données
        $user = $entityManager->getRepository(User::class)->findOneBy(['email' => 'test@example.com']);
        $this->assertTrue($encoder->isPasswordValid($user, 'new-password'));
    }

    public function testAdresse(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Créer un utilisateur et le persister en base de données
        $user = new User();
        $user->setEmail('test@example.com');
        $user->setPassword('password');
        $user->setRoles(['ROLE_USER']);
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');
        $entityManager->persist($user);
        $entityManager->flush();

        // Connecter le client en tant qu'utilisateur
        $client->loginUser($user);

        // Envoyer une requête GET à la route "app_profil"
        $client->request('GET', '/home/profil');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le formulaire de modification d'adresse est présent dans la réponse
        $this->assertSelectorExists('form[name=profil]');

        // Simuler la soumission du formulaire avec des données valides
        $client->request('POST', '/home/profil', [
            'profil' => [
                'address' => '123 Rue de la Paix',
                'city' => 'Paris',
                'zipCode' => '75000',
            ],
        ]);
    }
}