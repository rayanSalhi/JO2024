<?php

namespace App\Tests\Controller;

use App\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CartControllerTest extends WebTestCase
{
    private $client;
    private $container;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->container = $this->client->getContainer();
    }

    public function testIndex()
    {
        $this->client->request('GET', '/mon-panier');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorTextContains('h1', 'Mon panier');
    }

    public function testAdd()
    {
        // Créez un événement de test et enregistrez-le dans la base de données
        $event = new Event();
        $event->setName('Événement de test');
        $event->setPrice(10);
        // ...
        $entityManager = $this->container->get('doctrine')->getManager();
        $entityManager->persist($event);
        $entityManager->flush();

        $this->client->request('GET', '/mon-panier/add/solo/' . $event->getId());
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertResponseRedirects('/mon-panier');

        $this->client->followRedirect();

        $this->assertSelectorTextContains('tbody tr', 'Événement de test');
        $this->assertSelectorTextContains('tbody tr', '10 €');
    }

    public function testModifyCart()
    {
        // Créez un événement de test et enregistrez-le dans la base de données
        $event = new Event();
        $event->setName('Événement de test');
        $event->setPrice(10);
        // ...
        $entityManager = $this->container->get('doctrine')->getManager();
        $entityManager->persist($event);
        $entityManager->flush();

        // Ajoutez l'événement de test au panier
        $this->client->request('GET', '/mon-panier/add/solo/' . $event->getId());
        $this->client->followRedirect();

        // Modifiez la quantité de l'événement de test dans le panier
        $this->client->request('GET', '/mon-panier/remove/solo/' . $event->getId());
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertResponseRedirects('/mon-panier');

        $this->client->followRedirect();

        $this->assertSelectorTextContains('tbody tr', 'Événement de test');
        $this->assertSelectorTextContains('tbody tr', '20 €');

        // Supprimez l'événement de test du panier
        $this->client->request('GET', '/mon-panier/delete/solo/' . $event->getId());
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertResponseRedirects('/mon-panier');

        $this->client->followRedirect();

        $this->assertSelectorTextNotContains('tbody tr', 'Événement de test');
    }

    public function testEmpty()
    {
        // Créez un événement de test et enregistrez-le dans la base de données
        $event = new Event();
        $event->setName('Événement de test');
        $event->setPrice(10);
        $entityManager = $this->container->get('doctrine')->getManager();
        $entityManager->persist($event);
        $entityManager->flush();

        // Ajoutez l'événement de test au panier
        $this->client->request('GET', '/mon-panier/add/solo/' . $event->getId());
        $this->client->followRedirect();

        // Videz le panier
        $this->client->request('GET', '/mon-panier/empty');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
        $this->assertResponseRedirects('/mon-panier');

        $this->client->followRedirect();

        $this->assertSelectorTextNotContains('tbody tr', 'Événement de test');
    }
}
