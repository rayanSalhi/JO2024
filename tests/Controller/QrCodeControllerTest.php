<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class QrCodeControllerTest extends WebTestCase
{
    public function testPdf(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Remplacer $id par un ID de ticket valide dans votre base de données
        $client->request('GET', '/pdf/1');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le type de contenu de la réponse est application/pdf
        $this->assertResponseHeaderSame('Content-Type', 'application/pdf');

        // Vérifier que le contenu de la réponse n'est pas vide
        $this->assertNotEmpty($client->getResponse()->getContent());
    }

    public function testDecode(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Remplacer $key par une clé de QR code valide dans votre base de données
        $client->request('GET', '/qrcode/ABCDEFGHIJKLM');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le contenu de la réponse contient le message "TICKET OFFICIEL"
        $this->assertSelectorTextContains('body', 'TICKET OFFICIEL');
    }
}
