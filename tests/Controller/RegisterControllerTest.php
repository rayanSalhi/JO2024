<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class RegisterControllerTest extends WebTestCase
{
    public function testRegister(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Envoyer une requête GET à la route "app_register"
        $client->request('GET', '/inscription');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le contenu de la réponse contient le titre de la page
        $this->assertSelectorTextContains('h1', 'Inscription');

        // Remplir le formulaire d'inscription avec des données valides
        $client->submitForm('S\'inscrire', [
            'register[email]' => 'test@example.com',
            'register[Password]' => 'password123',
            'register[confirm_password]' => 'password123',
            // Ajoutez ici les autres champs de votre formulaire d'inscription
        ]);

        // Vérifier que la redirection vers la page d'accueil a bien été effectuée
        $this->assertResponseRedirects('/');

        // Vérifier que l'utilisateur a bien été enregistré en base de données
        // Vous pouvez par exemple récupérer l'utilisateur par son adresse e-mail et vérifier qu'il n'est pas null
    }
}
