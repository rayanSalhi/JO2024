<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;
use App\Entity\Event;

class OrderControllerTest extends WebTestCase
{
    public function testAdd(): void
    {
        // Créer un client pour simuler une requête HTTP
        $client = static::createClient();

        // Créer un utilisateur et le persister en base de données
        $user = new User();
        $user->setEmail('test@example.com');
        $user->setPassword('password');
        $user->setRoles(['ROLE_USER']);
        $entityManager = $client->getContainer()->get('doctrine.orm.entity_manager');
        $entityManager->persist($user);
        $entityManager->flush();

        // Créer un événement et le persister en base de données
        $event = new Event();
        $event->setName('Test Event');
        $event->setPrice(10);
        // ... ajouter d'autres propriétés si nécessaire
        $entityManager->persist($event);
        $entityManager->flush();

        // Connecter le client en tant qu'utilisateur
        $client->loginUser($user);

        // Ajouter un élément au panier en utilisant la session
        $session = $client->getContainer()->get('session');
        $panier = $session->get('panier', []);
        $panier[] = [
            'event_id' => $event->getId(),
            'type' => 'Solo',
            'price' => $event->getPrice(),
            'quantity' => 2,
        ];
        $session->set('panier', $panier);

        // Envoyer une requête GET à la route "app_order_add"
        $client->request('GET', '/commandes/ajout');

        // Vérifier que le code de statut de la réponse est 200 (OK)
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        // Vérifier que le contenu de la réponse contient le nom de l'événement et le prix total
        $this->assertSelectorTextContains('td', 'Test Event');
        $this->assertSelectorTextContains('strong', '20 €');
    }
}
